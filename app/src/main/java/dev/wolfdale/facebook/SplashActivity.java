package dev.wolfdale.facebook;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


public class SplashActivity extends AppCompatActivity {

    private static int TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final ImageView imgFbLogo = (ImageView) findViewById(R.id.img_fb_logo);
        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.animation);
        imgFbLogo.startAnimation(pulse);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(SplashActivity.this,
                                imgFbLogo,
                                ViewCompat.getTransitionName(imgFbLogo));
                startActivity(intent, options.toBundle());
            }
        }, TIME_OUT);

    }
}
